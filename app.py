import pygame
from config import font, sc

class Window:
    def __init__(self, dsnc):
        self.x = 0
        self.y = 0

        self.width = 20
        self.height = 20

        self.image = None
        self.rect = None

        self.get_window = False
        self.color = 255

        self.dsnc = dsnc

        self.name = ""

        self.shadow = None
    
    def create_surf(self):
        self.image = pygame.Surface((self.width, self.height))
        self.rect = self.image.get_rect(center=(self.x, self.y))
        self.shadow = pygame.Surface((self.width+20, self.height+23))
        self.shadow.set_alpha(115)
    
    def draw(self):
        pass
    
    def update(self):
        mouse_x, mouse_y = pygame.mouse.get_pos()

        txt_draw = pygame.font.Font(font, 16).render(str(self.name), False, (self.color, self.color, self.color))

        if self.get_window:
            self.rect.centerx = mouse_x
            self.rect.top = mouse_y - 15
        
        if self.rect.collidepoint(mouse_x, mouse_y):
            self.color = 255
            if pygame.key.get_pressed()[pygame.K_LALT] and pygame.key.get_pressed()[pygame.K_q]:
                self.dsnc.kill(self.name)
                return
        else:
            self.color = 155
        if not pygame.mouse.get_pressed()[0]:
            self.get_window = False
            self.dsnc.clear()

        sc.blit(self.shadow, self.shadow.get_rect(center=(self.rect.centerx+15, self.rect.centery+18)))
        self.image.fill((0, 0, 0))
        self.draw()
        pygame.draw.rect(self.image, (0, 0, 0), (0, 0, self.image.get_width(), 35))
        pygame.draw.rect(self.image, (self.color, self.color, self.color), (0, 0, self.image.get_width(), self.image.get_height()), 3)
        pygame.draw.rect(self.image, (self.color, self.color, self.color), (0, 0, self.image.get_width(), 35), 2)
        self.image.blit(txt_draw, txt_draw.get_rect(center=(self.image.get_width()/2, 17)))
