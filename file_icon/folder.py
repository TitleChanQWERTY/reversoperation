import pygame
from group_conf import icon_group
import file
from config import WINDOW_SIZE
import language

folder_icon = pygame.image.load("Assets/sprite/icon/folder.png").convert_alpha()
folder_icon = pygame.transform.scale(folder_icon, (15 * 3, 13 * 3))

class Folder(file.File):
    def __init__(self, x, y, dsnc, name=language.txt_lang["FILE_NAME_ICON"]):
        super().__init__(x, y, dsnc)

        self.image = folder_icon
        self.load_icon_pos(self.x, self.y)

        self.name = name
    def update(self):
        super().update()
