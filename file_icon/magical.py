import pygame
from group_conf import icon_group
import file
from config import WINDOW_SIZE
from software import magicaladriana
import language

ball_dash_icon = pygame.image.load("Assets/sprite/icon/magicaladriana.png").convert_alpha()
ball_dash_icon = pygame.transform.scale(ball_dash_icon, (14 * 3, 14 * 3))

class MagicalIcon(file.File):
    def __init__(self, x, y, dsnc):
        super().__init__(x, y, dsnc)

        self.image = ball_dash_icon
        self.load_icon_pos(self.x, self.y)

        self.name = "MagicalAdriana"
    
    def open_file(self):
        self.dsnc.add_window(magicaladriana.MagicalAdriana(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2, self.dsnc))

    def update(self):
        super().update()
