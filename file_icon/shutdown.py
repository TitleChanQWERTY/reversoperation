import pygame
from group_conf import icon_group
import file
from config import WINDOW_SIZE
import language
import sys

shutdown_icon = pygame.image.load("Assets/sprite/icon/shutdown.png").convert_alpha()
shutdown_icon = pygame.transform.scale(shutdown_icon, (16 * 3, 16 * 3))


class Shutdown(file.File):
    def __init__(self, x, y, dsnc):
        super().__init__(x, y, dsnc)

        self.image = shutdown_icon
        self.load_icon_pos(self.x, self.y)

        self.name = language.txt_lang["EXIT_TXT"]
    
    def open_file(self):
        pygame.quit()
        sys.exit()
    def update(self):
        super().update()
