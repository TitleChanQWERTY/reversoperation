import pygame
from group_conf import icon_group
import file
from config import WINDOW_SIZE
from software import ball_dash
import language

ball_dash_icon = pygame.image.load("Assets/sprite/icon/ball_dash.png").convert_alpha()
ball_dash_icon = pygame.transform.scale(ball_dash_icon, (15 * 3, 13 * 3))

class BallDashIcon(file.File):
    def __init__(self, x, y, dsnc):
        super().__init__(x, y, dsnc)

        self.image = ball_dash_icon
        self.load_icon_pos(self.x, self.y)

        self.name = "BallDash"
    
    def open_file(self):
        self.dsnc.add_window(ball_dash.BallDash(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2, self.dsnc))

    def update(self):
        super().update()
