import pygame
from group_conf import icon_group
import file
from config import WINDOW_SIZE
from software import text_editor
import language

class StoryFile(file.File):
    def __init__(self, x, y, dsnc, txt, number):
        super().__init__(x, y, dsnc)

        self.load_icon_pos(self.x, self.y)

        self.name = str(number)

        self.txt = txt
    
    def open_file(self):
        self.dsnc.add_window(text_editor.TextEditor(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2, self.dsnc, self.txt, (505, 630)))

    def update(self):
        super().update()
