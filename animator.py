class Animator:
    def __init__(self, timeout, filename):
        self.timeout = timeout
        self.filename = filename
        self.Frame  = 0
        self.time = 0

        self.last_item = len(self.filename)

        self.count_play = 0

    def update(self):
        if self.time >= self.timeout:
            self.Frame += 1
            self.time = 0
        if self.Frame > self.last_item - 1:
            self.count_play += 1
        self.Frame %= self.last_item
        self.time += 1

        return self.filename[self.Frame]
