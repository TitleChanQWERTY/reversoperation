import pygame
from config import sc, WINDOW_SIZE, font
import group_conf


class DSNC:
    def __init__(self):
        self.focus = None
        self.window_count = []

        self.debug_surf = pygame.Surface((WINDOW_SIZE[0], 22))

        self.time_show_debug = 320
        self.txt_debug = None

    def clear(self):
        self.focus = None
    def add_window(self, window):
        window.name.lower()
        self.window_count.append(window)
    def kill(self, name):
        mouse_x, mouse_y = pygame.mouse.get_pos()
        for b, window in enumerate(self.window_count):
            if self.window_count[b].rect.collidepoint(mouse_x, mouse_y) and not pygame.mouse.get_pressed()[0]:
                if name == self.window_count[b].name:
                    if name == "Magical Adriana 2000":
                        for bullet_e in group_conf.enemy_g:
                            bullet_e.kill()
                        for bullet_p in group_conf.player_g:
                            bullet_p.kill()
                        for enemy in group_conf.ma_enemy:
                            enemy.kill()
                    self.window_count.pop(b)
                    return
    def send_notf_debug(self, txt):
        if self.time_show_debug >= 75:
            self.time_show_debug = 0
            self.txt_debug = pygame.font.Font(font, 17).render("=> "+str(txt), False, (255, 25, 25))
    def update(self):
        mouse_x, mouse_y = pygame.mouse.get_pos()
        for b, window in enumerate(self.window_count):
            if self.window_count[b].rect.collidepoint(mouse_x, mouse_y) and self.focus is None:
                if pygame.mouse.get_pressed()[0] and mouse_y <= window.rect.top+34:
                    window_obj = self.window_count[b]
                    self.focus = self.window_count[b]
                    self.window_count[b].get_window = True
                    self.window_count.pop(b)
                    self.window_count.insert(len(self.window_count), window_obj)
                    return
            try:
                self.window_count[b].update()
                sc.blit(self.window_count[b].image, self.window_count[b].rect)
            except IndexError:
                return
        self.debug_surf.fill((0, 0, 0))
        if self.time_show_debug >= 144:
            self.txt_debug = pygame.font.Font(font, 17).render("> ", False, (255, 255, 255))
        else:
            self.time_show_debug += 1
        self.debug_surf.blit(self.txt_debug, (5, -2))
        sc.blit(self.debug_surf, self.debug_surf.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]-12)))
            
