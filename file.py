import pygame
from group_conf import icon_group
from config import font, sc, WINDOW_SIZE
import language

file_icon = pygame.image.load("Assets/sprite/icon/file.png").convert_alpha()
file_icon = pygame.transform.scale(file_icon, (15 * 3, 17 * 3))

class File(pygame.sprite.Sprite):
    def __init__(self, x, y, dsnc):
        super().__init__()
        self.x = x
        self.y = y

        self.image = file_icon
        self.rect = self.image.get_rect(center=(self.x, self.y))

        self.name = ""
        self.txt_font = pygame.font.Font(font, 15)

        self.dsnc = dsnc

        self.time_click = 100

        self.add(icon_group)
    
    def load_icon_pos(self, x, y):
        self.rect = self.image.get_rect(center=(x, y))

    def open_file(self):
        self.dsnc.send_notf_debug(language.txt_lang["FILE_NOT_FOUND"])
    
    def update(self):
        txt_name = self.txt_font.render(str(self.name), False, (255, 255, 255))
        mouse_x, mouse_y = pygame.mouse.get_pos()
        sc.blit(txt_name, txt_name.get_rect(center=(self.rect.centerx+1, self.rect.bottom+11)))

        if self.rect.collidepoint(mouse_x, mouse_y):
            if self.dsnc.focus is None:
                pygame.draw.rect(sc, (255, 255, 255), (self.rect.x-2, self.rect.y-4, self.image.get_width()+6, self.image.get_height()+6), 2)
                if self.time_click >= 12 and pygame.mouse.get_pressed()[0]:
                    self.time_click = 0
                    self.open_file()
                else:
                    self.time_click += 1
