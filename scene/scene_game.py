import pygame
from config import sc, WINDOW_SIZE, clock
from file_icon import folder, how_work, balldash, story_file, magical, wallpaper_icon, shutdown
from group_conf import icon_group
from dsnc import DSNC
from scene import scene_start_loading
from software import text_editor, wallpaper_select
import language
from screenshoot import take

catrios_icon = pygame.image.load("Assets/sprite/ui/icon_white.png").convert_alpha()

catrios_icon = pygame.transform.scale(catrios_icon, (55 * 2, 55 * 2))

catrios_icon_big = pygame.transform.scale(catrios_icon, (68 * 5, 68 * 5))

catrios_icon.set_alpha(170)
catrios_icon_big.set_alpha(170)

def _scene_():
    running = True
    FPS = 60

    dsnc_server = DSNC()

    dsnc_server.add_window(text_editor.TextEditor(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2, dsnc_server, language.txt_lang["Close_Window"]))

    folder.Folder(67, 75, dsnc_server, language.txt_lang["FILE_NAME_ICON"])
    balldash.BallDashIcon(67, 197, dsnc_server)
    magical.MagicalIcon(67, 330, dsnc_server)
    wallpaper_icon.WallpaperIcon(67, 457, dsnc_server)
    how_work.HowWork(WINDOW_SIZE[0]-100, 75, dsnc_server)
    story_file.StoryFile(WINDOW_SIZE[0]-100, 197, dsnc_server, language.txt_lang["README"], "README!")
    shutdown.Shutdown(WINDOW_SIZE[0]-100, WINDOW_SIZE[1]-100, dsnc_server)

    tick_event = 0

    sfx_click = pygame.mixer.Sound("Assets/sfx/click.ogg")
    sfx_click.set_volume(1)

    time_start_strange = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.MOUSEBUTTONDOWN:
                sfx_click.play()
            elif e.type == pygame.KEYDOWN and e.key == pygame.K_F6:
                take()
        
        sc.fill((18, 18, 18))
        if wallpaper_select.wallpaper == 1:
            pygame.draw.rect(sc, (255, 255, 255), (WINDOW_SIZE[0]/3+75, WINDOW_SIZE[1]/3-25, 290, 290), 2)
        elif wallpaper_select.wallpaper == 2:
            pygame.draw.circle(sc, (255, 255, 255), (WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2), 190, 3)
        elif wallpaper_select.wallpaper == 3:
            sc.blit(catrios_icon, catrios_icon.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2)))
        elif wallpaper_select.wallpaper == 4:
            sc.blit(catrios_icon_big, catrios_icon_big.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2)))
        icon_group.draw(sc)
        icon_group.update()
        dsnc_server.update()
        pygame.draw.rect(sc, (255, 255, 255), (pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1]-10, 10, 18))
        if time_start_strange >= 875:
            tick_event += 1
            if tick_event == 1305:
                dsnc_server.send_notf_debug(language.txt_lang["HELP_ME"])
            elif tick_event == 2495:
                dsnc_server.send_notf_debug(language.txt_lang["HE_KILL_ME"])
            elif tick_event == 3000:
                dsnc_server.send_notf_debug(language.txt_lang["Maybe_I_was"])
            elif tick_event == 3825:
                dsnc_server.send_notf_debug(language.txt_lang["NEW_FILE"])
                story_file.StoryFile(495, 450, dsnc_server, language.txt_lang["STORY_01"], "01")
            elif tick_event == 5677:
                dsnc_server.send_notf_debug(language.txt_lang["NEW_FILE"])
                story_file.StoryFile(200, 250, dsnc_server, language.txt_lang["STORY_02"], "02")
            elif tick_event == 6985:
                dsnc_server.send_notf_debug(language.txt_lang["NEW_FILE"])
                story_file.StoryFile(700, 150, dsnc_server, language.txt_lang["STORY_03"], "03")
            elif tick_event == 8190:
                dsnc_server.send_notf_debug(language.txt_lang["NEW_FILE"])
                story_file.StoryFile(800, 400, dsnc_server, language.txt_lang["STORY_04"], "04")
            elif tick_event == 10155:
                dsnc_server.send_notf_debug(language.txt_lang["NEW_FILE"])
                story_file.StoryFile(1000, 350, dsnc_server, language.txt_lang["STORY_05"], "05")
            elif tick_event == 11099:
                dsnc_server.send_notf_debug(language.txt_lang["NEW_FILE"])
                story_file.StoryFile(130, 630, dsnc_server, language.txt_lang["STORY_06"], "06")
            if pygame.key.get_pressed()[pygame.K_LALT] and pygame.key.get_pressed()[pygame.K_d] and pygame.key.get_pressed()[pygame.K_c]:
                running = False
                scene_start_loading.start_loading(scene_start_loading.game_end)
                return
            tick_event = max(0, min(11999, tick_event))
        else:
            time_start_strange += 1
        pygame.display.update()
        clock.tick(FPS)
