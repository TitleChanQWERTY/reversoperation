import pygame
from config import sc, WINDOW_SIZE, clock, font
from scene import scene_game
import user
import os
import sys
import shutil
import language
from screenshoot import take

def ihrovary():
    running = True
    FPS = 60

    img = pygame.image.load("Assets/sprite/ui/game_jam.png").convert_alpha()

    alpha = 0

    time_end = 0

    sfx_start = pygame.mixer.Sound("Assets/sfx/notf_call.ogg")
    sfx_start.play()

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
        
        sc.fill((0, 0, 0))
        if time_end >= 220:
            running = False
            start_loading()
            return
        time_end += 1
        img.set_alpha(alpha)
        alpha += 9
        sc.blit(img, img.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2)))
        pygame.display.update()
        clock.tick(FPS)

def start_scene():
    running = True
    FPS = 60

    mem = 0

    time_show = 0

    pick_sfx = pygame.mixer.Sound("Assets/sfx/bios.ogg")

    txt_copyrigth = pygame.font.Font(font, 14).render("Copyright (C) Andrii Komalovs/TitleChanQWERTY 1999-2007", False,  (255, 255, 255))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.KEYDOWN and e.key == pygame.K_F6:
                take()
        
        sc.fill((0, 0, 0))
        if time_show == 142:
            pick_sfx.play()
            time_show += 1
        elif time_show >= 142:
            if mem >= 450:
                running = False
                start_loading(ihrovary)
                return
            txt_loading_mem = pygame.font.Font(font, 13).render("-- Loading MEM: "+str(mem)+"/450 --\n================", False, (255, 255, 255))
            mem += 1
            sc.blit(txt_loading_mem, (14, 25))
            sc.blit(txt_copyrigth, txt_copyrigth.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]-10)))
        else:
            time_show += 1
        pygame.display.update()
        clock.tick(FPS)

def select_language():
    running = True
    FPS = 60

    select = 0

    txt_lang_select = pygame.font.Font(font, 17).render("-- Select Language --\n==============", False, (255, 255, 255))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_DOWN:
                    select += 1
                elif e.key == pygame.K_UP:
                    select -= 1
                elif e.key == pygame.K_RETURN:
                    running = False
                    language.select_lang = select
                    language.set_language()
                    start_loading(create_all)
                    return
                select %= 2
        
        sc.fill((0, 0, 0)) 
        sc.blit(txt_lang_select, (5, 15))     
        if select == 0:
            txt_lang_eng = pygame.font.Font(font, 15).render("English", False, (0, 0, 0))   
            txt_lang_ukr = pygame.font.Font(font, 15).render("Українська", False, (255, 255, 255))
            pygame.draw.rect(sc, (255, 255, 255), (0, txt_lang_eng.get_rect(center=(55, 85)).y, 185, 20)) 
        elif select == 1:
            txt_lang_eng = pygame.font.Font(font, 15).render("English", False, (255, 255, 255))
            txt_lang_ukr = pygame.font.Font(font, 15).render("Українська", False, (0, 0, 0))  
            pygame.draw.rect(sc, (255, 255, 255), (0, txt_lang_ukr.get_rect(center=(55, 125)).y, 185, 20))  
        sc.blit(txt_lang_eng, txt_lang_eng.get_rect(center=(93, 85))) 
        sc.blit(txt_lang_ukr, txt_lang_ukr.get_rect(center=(93, 125))) 
        pygame.display.update()
        clock.tick(FPS)

def start_loading(scene_switch=select_language):
    running = True
    FPS = 60

    time_load = 0

    frame_anim = 0 

    txt_draw = ""

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
        
        sc.fill((0, 0, 0))
        if frame_anim >= 10:
            txt_draw = "_"
        if frame_anim >= 22:
            txt_draw = ""
        if frame_anim >= 30:
            frame_anim = 0
        frame_anim += 1
        time_load += 1
        if time_load >= 170:
            running = False
            scene_switch()
            return
        elif time_load <= 135:
            txt_load = pygame.font.Font(font, 27).render(txt_draw, False, (255, 255, 255))
            sc.blit(txt_load, (5, 1))
        pygame.display.update()
        clock.tick(FPS)

def create_all():
    running = True
    FPS = 60

    select = 0

    user_name = ""
    password = ""

    txt_create_user = pygame.font.Font(font, 17).render("-- "+language.txt_lang["CREATE_USER"]+" --", False, (255, 255, 255))  
    txt_create_password = pygame.font.Font(font, 17).render("-- "+language.txt_lang["CREATE_PASSWORD"]+" --", False, (255, 255, 255))  

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_RETURN:
                    if select < 1:
                        if len(user_name) >= 1:
                            select += 1
                    else:
                        if len(password) >= 1:
                            user.name = user_name
                            user.password = password
                            running = False
                            start_loading(scene_game._scene_)
                            return
                if select == 0:
                    if not e.key == pygame.K_SPACE and not e.key == pygame.K_RETURN and not e.key == pygame.K_TAB and not e.key == pygame.K_ESCAPE:
                        if e.key == pygame.K_BACKSPACE:
                            user_name = user_name[:-1]
                        else:
                            user_name += e.unicode
                elif select == 1:
                    if not e.key == pygame.K_SPACE and not e.key == pygame.K_RETURN and not e.key == pygame.K_TAB and not e.key == pygame.K_ESCAPE:
                        if e.key == pygame.K_BACKSPACE:
                            password = password[:-1]
                        else:
                            password += e.unicode
                    
        
        sc.fill((0, 0, 0))
        if select == 0:
            txt_user = pygame.font.Font(font, 16).render(str(user_name)+"_", False, (255, 255, 255))    
            sc.blit(txt_create_user, (9, 10))    
            sc.blit(txt_user, (10, 65))  
        elif select == 1:
            txt_user = pygame.font.Font(font, 16).render(str(password)+"_", False, (255, 255, 255))    
            sc.blit(txt_create_password, (9, 10))    
            sc.blit(txt_user, (10, 65)) 
        pygame.display.update()
        clock.tick(FPS)

def game_end():
    running = True
    FPS = 60

    txt_delete_catrin = pygame.font.Font(font, 18).render("-- "+language.txt_lang["Delete_Catrin"]+" --\n============", False, (255, 255, 255))
    accept_font = pygame.font.Font(font, 17)

    txt_accept = ""

    sfx_end = pygame.mixer.Sound("Assets/sfx/end_sfx.ogg")
    sfx_end.set_volume(0.1)

    sfx_end.play(-1)

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_y:
                    txt_accept = "Y"
                elif e.key == pygame.K_n:
                    txt_accept = "N"
                elif e.key == pygame.K_BACKSPACE:
                    txt_accept = ""
                elif e.key == pygame.K_RETURN:
                    if txt_accept == "Y":
                        running = False
                        close_remove()
                        return
                    elif txt_accept == "N":
                        running = False
                        start_loading(end_no)
                        return
                        
        
        sc.fill((0, 0, 0))
        accept = accept_font.render("Y/N: "+txt_accept, False, (255, 255, 255))
        sc.blit(accept, (7, 85))
        sc.blit(txt_delete_catrin, (6, 11))
        pygame.display.update()
        clock.tick(FPS)

def close_remove():
    running = True
    FPS = 60

    txt_password_info = pygame.font.Font(font, 19).render(language.txt_lang["PASSWORD_P"], False, (255, 255, 255))

    password = ""

    password_font = pygame.font.Font(font, 17)

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.KEYDOWN:
                if not e.key == pygame.K_SPACE and not e.key == pygame.K_RETURN and not e.key == pygame.K_TAB and not e.key == pygame.K_ESCAPE:
                    if e.key == pygame.K_BACKSPACE:
                        password = password[:-1]
                    else:
                        password += e.unicode
                elif e.key == pygame.K_RETURN and password == user.password:
                    with open("README.txt", "w") as file:
                        file.write(language.txt_lang["END_THANKS"])
                    shutil.rmtree("Assets/sfx")
                    shutil.rmtree("Assets/sprite")
                    shutil.rmtree("language")
                    pygame.quit()
                    sys.exit()
                    return
        
        sc.fill((0, 0, 0))
        sc.blit(txt_password_info, (7, 12))
        password_txt = password_font.render(str(password)+"_", False, (255, 255, 255))
        sc.blit(password_txt, (7, 65))
        pygame.display.update()
        clock.tick(FPS)

    

def end_no():
    running = True
    FPS = 60

    txt_fuck = pygame.font.Font(font, 19).render(language.txt_lang["END_NO"], False, (255, 255, 255))

    time_end = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
        
        sc.fill((0, 0, 0))
        if time_end >= 120:
            pygame.quit()
            sys.exit()
        time_end += 1
        sc.blit(txt_fuck, (7, 15))
        pygame.display.update()
        clock.tick(FPS)
