from app import Window
from random import randint
from config import font
import pygame

pickup = pygame.mixer.Sound("Assets/sfx/pickupCoin.ogg")

class Ball:
    def __init__(self, y, x, speed):
        self.rect = pygame.Rect(x, y, 13, 13)
        self.speed = speed

    def draw(self, surf):
        self.rect.y += self.speed
        pygame.draw.circle(surf, (255, 255, 255), self.rect.center, 14)

class BallDash(Window):
    def __init__(self, x, y, dsnc):
        super().__init__(dsnc)

        self.x = x
        self.y = y

        self.width = 300
        self.height = 450

        self.name = "BallDash"

        self.rect_take = pygame.Rect(self.width/2-30, self.height-50, 65, 10)

        self.time_create = 0
        self.max_time_create = randint(85, 105)

        self.ball_mass = []

        self.font_count = pygame.font.Font(font, 18)

        self.count = 0

        self.create_surf()

        self.time_show_txt = 0

    def draw(self):
        mouse_x, mouse_y = pygame.mouse.get_pos()
        if self.rect.collidepoint(mouse_x, mouse_y):
            if self.time_show_txt <= 135:
                txt_contorl = self.font_count.render("MOVE: <ARROW>", False, (255, 255, 255))
                self.image.blit(txt_contorl, txt_contorl.get_rect(center=(self.width/2, self.height/2)))
                self.time_show_txt += 1
            if self.time_create >= self.max_time_create:
                self.max_time_create = randint(65, 90)
                self.time_create = 0
                self.ball_mass.append(Ball(-25, randint(7, self.width-10), randint(2, 6)))
            else:
                self.time_create += 1
            for b, ball in enumerate(self.ball_mass):
                self.ball_mass[b].draw(self.image)
                if self.ball_mass[b].rect.colliderect(self.rect_take):
                    self.ball_mass.pop(b)
                    pickup.play()
                    self.count += 1
                    return
            pygame.draw.rect(self.image, (255, 255, 255), self.rect_take)
            if pygame.key.get_pressed()[pygame.K_LEFT]:
                self.rect_take.centerx -= 5
            elif pygame.key.get_pressed()[pygame.K_RIGHT]:
                self.rect_take.centerx += 5
        self.txt_count = self.font_count.render(str(self.count), False, (self.color, self.color, self.color))
        self.image.blit(self.txt_count, self.txt_count.get_rect(center=(self.width/2, 55)))
        
    def update(self):
        super().update()
