import pygame
from app import Window
from config import font

class TextEditor(Window):
    def __init__(self, x, y, dsnc, txt, size=(550, 170)):
        super().__init__(dsnc)

        self.x = x
        self.y = y

        self.width = size[0]
        self.height = size[1]

        self.name = "TextViewer"

        self.font_p = pygame.font.Font(font, 20)

        self.text_lines = []

        self.create_surf()

        self.yPosMass = []
        self.load_txt(txt)

    def load_txt(self, txt):
        words = txt.split(" ")
        current_line = words[0]

        yPos = 60

        for word in words[1:]: 
            rendered_word = self.font_p.render(current_line + " " + word, False, (255, 255, 255))
            if rendered_word.get_width() < self.image.get_width() - 20:
                current_line += " " + word
            else:
                self.text_lines.append(current_line)
                current_line = word
            self.yPosMass.append(yPos)
            yPos += 28
        self.text_lines.append(current_line)

    def update(self):
        super().update()
        for i, line in enumerate(self.text_lines):
            rendered_line = self.font_p.render(line, False, (self.color, self.color, self.color))
            self.image.blit(rendered_line, rendered_line.get_rect(
                center=(self.image.get_width()/2, self.yPosMass[i])))
