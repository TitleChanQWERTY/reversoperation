from app import Window
import pygame
from config import font
import language

wallpaper = 0

catrios_icon = pygame.image.load("Assets/sprite/ui/icon_white.png").convert_alpha()

catrios_icon = pygame.transform.scale(catrios_icon, (55, 55))

catrios_icon_big = pygame.transform.scale(catrios_icon, (67 * 2, 67 * 2))

catrios_icon.set_alpha(180)
catrios_icon_big.set_alpha(180)

class WallpaperSelect(Window):
    def __init__(self, x, y, dsnc):
        super().__init__(dsnc)
        self.x = x
        self.y = y

        self.width = 465
        self.height = 385
        self.create_surf()

        self.name = "Wallpaper"

        self.surf_wallpapet = pygame.Surface((350, 270))

        self.font = pygame.font.Font(font, 17)

        self.wallpaper_name = "None"

        self.time_switch = 0

        self.txt_arrow_font = pygame.font.Font(font, 16)
    
    def draw(self):
        global wallpaper
        mouse_x, mouse_y = pygame.mouse.get_pos()
        txt_wallpaper = self.font.render("< "+str(self.wallpaper_name)+" >", False, (self.color, self.color, self.color))
        self.image.blit(txt_wallpaper, txt_wallpaper.get_rect(center=(self.width/2, self.height/2-140)))
        self.surf_wallpapet.fill((18, 18, 18))
        txt_arrow = self.txt_arrow_font.render(language.txt_lang["USING_ARROW"], False, (self.color, self.color, self.color))
        if wallpaper == 0:
            self.wallpaper_name = "None"
        elif wallpaper == 1:
            self.wallpaper_name = "Cube"
            pygame.draw.rect(self.surf_wallpapet, (255, 255, 255), (self.surf_wallpapet.get_width()/3-5, self.surf_wallpapet.get_height()/3-20, 125, 125), 2)
        elif wallpaper == 2:
            self.wallpaper_name = "Circle"
            pygame.draw.circle(self.surf_wallpapet, (255, 255, 255), (self.surf_wallpapet.get_width()/2, self.surf_wallpapet.get_height()/2), 70, 2)
        elif wallpaper == 3:
            self.wallpaper_name = "CatriOS Small"
            self.surf_wallpapet.blit(catrios_icon, catrios_icon.get_rect(center=(self.surf_wallpapet.get_width()/2, self.surf_wallpapet.get_height()/2)))
        elif wallpaper == 4:
            self.wallpaper_name = "CatriOS Big"
            self.surf_wallpapet.blit(catrios_icon_big, catrios_icon_big.get_rect(center=(self.surf_wallpapet.get_width()/2, self.surf_wallpapet.get_height()/2)))
        self.image.blit(txt_arrow, txt_arrow.get_rect(center=(self.width/2, self.height-14)))
        self.image.blit(self.surf_wallpapet, self.surf_wallpapet.get_rect(center=(self.width/2, self.height/2+13)))
        if self.rect.collidepoint(mouse_x, mouse_y):
            if self.time_switch >= 10:
                if pygame.key.get_pressed()[pygame.K_LEFT]:
                    wallpaper -= 1
                    self.time_switch = 0
                elif pygame.key.get_pressed()[pygame.K_RIGHT]:
                    wallpaper += 1
                    self.time_switch = 0
            else:
                self.time_switch += 1
            wallpaper %= 5
            

    def update(self):
        super().update()
