from app import Window
from random import randint
from config import font, sc
from animator import Animator
from group_conf import ma_enemy, enemy_g, player_g
import pygame

pickup = pygame.mixer.Sound("Assets/sfx/pickupCoin.ogg")
enemy_die = pygame.mixer.Sound("Assets/sfx/enemy_die.ogg")
player_shoot = pygame.mixer.Sound("Assets/sfx/player_shoot.ogg")
player_damage = pygame.mixer.Sound("Assets/sfx/PLAYER_TAKE_DAMAGE_SECOND.ogg")

protator = [pygame.image.load("Assets/sprite/enemy/protator/1.png").convert_alpha(),
            pygame.image.load("Assets/sprite/enemy/protator/2.png").convert_alpha()]

for i in range(2):
    protator[i] = pygame.transform.scale(protator[i], (12 * 2, 23 * 2))
  

player_bullet = pygame.image.load("Assets/sprite/bullet/player/1.png").convert_alpha()
player_bullet = pygame.transform.scale(player_bullet, (7 * 2, 8 * 2))

enemy_bullet = pygame.image.load("Assets/sprite/bullet/enemy/1.png").convert_alpha()
enemy_bullet = pygame.transform.scale(enemy_bullet, (6 * 2, 6 * 2))

player_img = pygame.image.load("Assets/sprite/player/1.png").convert_alpha()
player_img = pygame.transform.scale(player_img, (10 * 2, 13 * 2))

class Enemy(pygame.sprite.Sprite):
    def __init__(self, x, surf):
        super().__init__()

        self.image = protator[0]
        self.rect = self.image.get_rect(center=(x, -25))

        self.max_down = randint(55, 330)

        self.anim = Animator(15, protator)

        self.time_shoot = 0
        self.surf = surf
        self.health = randint(15, 25)

        self.add(ma_enemy)
    
    def update(self):
        self.rect.y += 4
        self.rect.y = max(-35, min(self.max_down, self.rect.y))
        self.image = self.anim.update()
        if self.time_shoot >= 95:
            self.time_shoot = 0
            speed_x = randint(-5, 5)
            speed_y = randint(-5, 5)
            if speed_y == 0:
                speed_y = 1
            Bullet(self.rect.centerx, self.rect.centery, (speed_x, speed_y), enemy_bullet, self.surf)
        self.time_shoot += 1

class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y, speed, surf, draw_surf, group=enemy_g):
        super().__init__()
        self.image = surf
        self.rect = self.image.get_rect(center=(x, y))

        self.speed = speed
        self.draw_surf = draw_surf

        self.add(group)

    def update(self):
        self.rect.move_ip(self.speed)
        if self.rect.y <= -15 or self.rect.y >= self.draw_surf.get_height() or self.rect.x <= -15 or self.rect.x >= self.draw_surf.get_width():
            self.kill()

class Player:
    def __init__(self, x, y, surf):
        self.image = player_img
        self.rect = self.image.get_rect(center=(x, y))

        self.surf = surf

        self.speed = 5

        self.AMMO = 0

        self.time_shoot = 0

        self.life = 3

        self.time_damage = 100
    
    def update(self):
        key = pygame.key.get_pressed()
        self.move(key)
        self.rect.centerx = max(10, min(self.surf.get_width()-10, self.rect.centerx))
        self.rect.centery = max(5, min(self.surf.get_height()-13, self.rect.centery))
        self.shoot(key)
        self.time_damage -= 1
        self.time_damage = max(0, min(100, self.time_damage))
        if self.time_damage > 0:
            self.image.set_alpha(100)
        else:
            self.image.set_alpha(300)

    def shoot(self, key):
        if key[pygame.K_x]:
            if self.time_shoot >= 10:
                self.time_shoot = 0
                Bullet(self.rect.centerx, self.rect.y, (0, -9), player_bullet, self.surf, player_g)
                player_shoot.play()
            else:
                self.time_shoot += 1

    def move(self, key):
        if key[pygame.K_LEFT]:
            self.rect.x -= self.speed
        elif key[pygame.K_RIGHT]:
            self.rect.x += self.speed

        if key[pygame.K_UP]:
            self.rect.y -= self.speed
        elif key[pygame.K_DOWN]:
            self.rect.y += self.speed


class MagicalAdriana(Window):
    def __init__(self, x, y, dsnc):
        super().__init__(dsnc)

        self.x = x
        self.y = y

        self.width = 500
        self.height = 450

        self.create_surf()

        self.name = "Magical Adriana 2000"

        self.time_create = 0
        self.max_time_create = randint(90, 100)

        self.font_count = pygame.font.Font(font, 19)

        self.count = 0

        self.enemy_mass = []

        self.Score = 0

        self.player = Player(self.image.get_width()/2, self.image.get_height()-50, self.image)

        self.time_show_txt = 0

    def draw(self):
        mouse_x, mouse_y = pygame.mouse.get_pos()
        txt_score = self.font_count.render("SCORE: "+str(self.Score), False, (self.color, self.color, self.color))
        txt_life = self.font_count.render("LIFE: "+str(self.player.life), False, (self.color, self.color, self.color))
        if self.rect.collidepoint(mouse_x, mouse_y):
            if self.time_show_txt <= 140:
                txt_contorl = self.font_count.render("MOVE: ARROW\n    SHOOT: X", False, (255, 255, 255))
                self.image.blit(txt_contorl, txt_contorl.get_rect(center=(self.width/2, self.height/2)))
                self.time_show_txt += 1
            if self.time_create >= self.max_time_create:
                self.enemy_mass.append(Enemy(randint(8, self.width-10), self.image))
                self.time_create = 0
            self.time_create += 1

            self.image.blit(self.player.image, self.player.rect)
            self.player.update()

            player_g.draw(self.image)
            ma_enemy.draw(self.image)
            enemy_g.draw(self.image)

            ma_enemy.update()
            player_g.update()
            enemy_g.update()

            for enemy in ma_enemy:
                for bullet in player_g:
                    if bullet.rect.collidepoint(enemy.rect.center):
                        enemy.health -= randint(6, 7)
                        enemy.rect.y -= 9
                        if enemy.health <= 0:
                            enemy_die.play()
                            enemy.kill()
                            self.Score += randint(5, 10)
                        bullet.kill()
            if self.player.time_damage <= 0:
                for bullet_es in enemy_g:
                    if bullet_es.rect.collidepoint(self.player.rect.center):
                        self.player.rect.y += 7
                        player_damage.play()
                        self.player.life -= 1
                        self.player.time_damage = 100
                        bullet_es.kill()
                        return
            if self.player.life < 0:
                self.dsnc.kill(self.name)
        self.image.blit(txt_score, txt_score.get_rect(center=(self.width/2+2, 55)))
        self.image.blit(txt_life, txt_life.get_rect(center=(self.width/5, 55)))

           
        
    def update(self):
        super().update()
