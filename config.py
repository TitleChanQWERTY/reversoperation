from pygame import time, display, init, HWSURFACE, FULLSCREEN, SCALED

init()

WINDOW_SIZE = (1280, 720)

sc = display.set_mode(WINDOW_SIZE, HWSURFACE | FULLSCREEN | SCALED)

clock = time.Clock()

font = "Assets/font/AdLib_Win95BT.ttf"
