from app import Window
import pygame

class Konsole(Window):
    def __init__(self, x, y, dsnc):
        super().__init__(dsnc)
        self.x = x
        self.y = y

        self.width = 340
        self.height = 250
        self.create_surf()

        self.name = "konsole"
    
    def update(self):
        super().update()
