import json
import os


def check():
    LANG = os.listdir("language")
    return LANG


LANGUAGE = check()

select_lang = 0

txt_lang = None


def set_language():
    global txt_lang
    with open("language/" + LANGUAGE[select_lang], encoding='utf-8') as lang:
        txt_lang = json.load(lang)
    return 


set_language()
