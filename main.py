from pygame import display, mouse, quit, image
from scene import scene_game, scene_start_loading

mouse.set_visible(False)

display.set_icon(image.load("Assets/sprite/icon_app/1.png").convert_alpha())

display.set_caption("CatriOS")

if __name__ == "__main__":
   scene_start_loading.start_scene()
quit()
